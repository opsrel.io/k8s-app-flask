FROM python:3.9-slim

LABEL app="k8s python flask demo application"
LABEL maintainer="Rémi Verchère <remi@verchere.fr>"
LABEL major_version="2"
LABEL minor_version="0"

ARG COLOR="white"

RUN apt-get update && \
    apt-get install -y stress && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

# Get requirements for python application
COPY app/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV FLASK_APP=/usr/src/app/app.py
ENV FLASK_APP_PORT=8080
ENV FLASK_APP_HOST="0.0.0.0"
ENV FLASK_APP_LOG="--access-logfile"
ENV FLASK_APP_LOG_FILE="'-'"
ENV FLASK_APP_COLOR="$COLOR"

ENV GUNICORN_CMD_ARGS "--bind=$FLASK_APP_HOST:$FLASK_APP_PORT $FLASK_APP_LOG $FLASK_APP_LOG_FILE"

# Copy and run it
COPY app .

CMD [ "gunicorn", "app:app"]
